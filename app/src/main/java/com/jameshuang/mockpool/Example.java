package com.jameshuang.mockpool;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;

/**
 * @auther James Huang
 * @date 2018/5/7
 */

public class Example {

    private Tester mTester;

    public Example (Tester tester) {
        mTester = tester;
    }

    public Intent dynamicDeclare () {
        Intent ret = new Intent();
        ret.putExtra("testKey", "test");
        ret.addCategory("test");
        return ret;
    }

    public void staticFunction () {
        Bitmap bmp = Bitmap.createBitmap(10, 10, Bitmap.Config.RGB_565);
        bmp.recycle();
    }

    public void ayncTask () {
        new MyAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    public interface Tester {
        public void fun1 ();

        public void fun2 ();

        public void fun3 ();
    }

    private class MyAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground (Void... voids) {
            mTester.fun1();
            return null;
        }

        @Override
        protected void onPreExecute () {
            super.onPreExecute();
            mTester.fun2();
        }

        @Override
        protected void onPostExecute (Void aVoid) {
            super.onPostExecute(aVoid);
            mTester.fun3();
        }
    }
}
