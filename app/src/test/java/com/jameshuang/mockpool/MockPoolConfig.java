package com.jameshuang.mockpool;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.jameshuang.mockpool.annotation.MockPoolField;

/**
 * @auther James Huang
 * @date 2018/5/7
 */

public class MockPoolConfig {
    @MockPoolField
    Intent intent;
    @MockPoolField("MockBmp")
    Bitmap bitmap;
    @MockPoolField
    AsyncTask asyncTask;
}
