package com.jameshuang.mockpool;

import android.graphics.Bitmap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @auther James Huang
 * @date 2018/5/4
 */

public class ExampleTest {

    @Mock
    Example.Tester mTester;

    private Example mExample;

    @Before
    public void setup () {
        MockitoAnnotations.initMocks(this);
        MockPool.initialize();

        mExample = new Example(mTester);
    }

    @Test
    public void dynamicDeclare_Normal () {
        mExample.dynamicDeclare();

        verify(MockPool.intent, times(1)).putExtra(anyString(), anyString());
        verify(MockPool.intent, times(1)).addCategory(anyString());
    }

    @Test
    public void staticFunction_Normal () {
        Bitmap bmp = Mockito.mock(Bitmap.class);
        when(MockPool.bitmap.createBitmap(anyInt(), anyInt(), (Bitmap.Config) any())).thenReturn(bmp);

        mExample.staticFunction();

        verify(MockPool.bitmap, times(1)).createBitmap(anyInt(), anyInt(), (Bitmap.Config) any());
        verify(bmp, times(1)).recycle();
    }

    @Test
    public void ayncTask_Normal () {
        mExample.ayncTask();

        verify(mTester, times(1)).fun1();
        verify(mTester, times(1)).fun2();
        verify(mTester, times(1)).fun3();
    }
}
