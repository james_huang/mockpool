Download
--------

project gradle:

```groovy
repositories {
    maven {
        url 'https://dl.bintray.com/jameshuang/mockpool'
    }
}
```

app gradle:

```groovy
dependencies {
    // you should work with the mockito
    //testImplementation 'org.mockito:mockito-core:2.8.9'

    testImplementation 'com.jameshuang.mockpool:mockpool-annotation:1.1.0'
    testAnnotationProcessor 'com.jameshuang.mockpool:mockpool-compile:1.3.0'
}
```

Check the latest version on https://bintray.com/jameshuang/mockpool.

Usage
------

In test class, add an annotation to the field that wanna test.

Notice that there can only annotate one field with the same Type, if you have several classes with the same simple name, you should set the value for the annotation for naming the mock class.

```java
public class MockPoolConfig {
    @MockPoolField
    Intent intent;
    @MockPoolField("MockBmp")
    Bitmap bitmap;
}
```

After IDE is built, there will generate the MockPool class, then you can initialize and get the mock instance from it.

```java
public class ExampleTest {
    @Before
    public void setup () {
        MockPool.initialize();
    }
    
    @Test
    public void staticFunction_Normal () {
        Bitmap bmp = Mockito.mock(Bitmap.class);
        when(MockPool.bitmap.createBitmap(anyInt(), anyInt(), (Bitmap.Config) any())).thenReturn(bmp);
        
        ...
    }
}
```